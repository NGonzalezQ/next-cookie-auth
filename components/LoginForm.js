import React, { useState } from 'react'
import { loginUser } from '../lib/auth'

const LoginForm = () => {
  const [email, setEmail] = useState('Sincere@april.biz')
  const [password, setPassword] = useState('hildegard.org')

  const handleChange = (setFunction, event) => {
    const value = event.target.value
    setFunction(value)
  }

  const handleSubmit = event => {
    event.preventDefault()
    loginUser(email, password)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <input
          type="email"
          name="email"
          placeholder="email"
          value={email}
          onChange={(e) => handleChange(setEmail, e)}
        />
      </div>
      <div>
        <input
          type="password"
          name="password"
          placeholder="password"
          value="password"
          onChange={(e) => handleChange(setPassword, e)}
        />
      </div>
      <button type="submit">Submit</button>
    </form>
  )
}

export default LoginForm
